package models

import (
	"net/http"
	"html/template"
	"database/sql"
	"fmt"
	"time"
	_ "github.com/go-sql-driver/mysql"
)

type Page struct{
	PageTitle string
	Data *User
	LoggedOn bool
}

type User struct{
	Id          string `json:"id"`
	Username    string `json:"username"`
	Pass        string `json:"password"`
	FullName    string `json:"fullname"`
	Phone       string `json:"telephone"`
	Address     string `json:"address"`
}

func GetUserByUsername(username string) (*User, error) {

	aUser := new(User)

	sqlStatement := `SELECT id, username, fullname, phone, address, password FROM users WHERE username LIKE "%` + username +  `%"`

	row := db.QueryRow(sqlStatement)

	err := row.Scan(&aUser.Id, &aUser.Username, &aUser.FullName, &aUser.Phone, &aUser.Address, &aUser.Pass)

	switch err {
		case sql.ErrNoRows:
			fmt.Println(err)
		 	fmt.Println("No rows were returned!")
		  return nil, err
		case nil:
		default:
			fmt.Println(err)
		 	panic(err)
	}

	return aUser, nil
}

func AddUser(aUser *User) (string, error) {

	userInsert, err := db.Prepare("INSERT INTO users (id, username, fullname, phone, address, password) VALUES (?, ?, ?, ?, ?, ?)")

	if err != nil {
		panic(err.Error())
		return "", err
	}

	defer userInsert.Close()

	_, err = userInsert.Exec(aUser.Id, aUser.Username, aUser.FullName, aUser.Phone, aUser.Address, aUser.Pass)

	if err != nil {
		panic(err.Error())
		return "", err
	}

	return aUser.Username, nil
}

func Login (username, password string) (bool) {

	var qty int

	sqlStatement := `SELECT COUNT(id) FROM users WHERE username='` + username + `' AND password='` + password + `';`

	row := db.QueryRow(sqlStatement)

	err := row.Scan(&qty)

	switch err {
		case sql.ErrNoRows:
			fmt.Println(err)
		 	fmt.Println("No rows were returned!")
		  return false
		case nil:
		default:
			fmt.Println(err)
		 	panic(err)
	}

	if qty == 1 {
		return true
	}

	return false

}

func UpdateUser(aUser *User) (*User, error) {

	sqlStatement := `UPDATE users SET username = '` + aUser.Username + `', fullname = '` + aUser.FullName + `', phone = '` + aUser.Phone + `', address = '`+ aUser.Address + `', password = '` + aUser.Pass + `' WHERE id = ` + aUser.Id + `;`

	_, err := db.Query(sqlStatement)

	if err != nil {
		panic(err.Error())
		return nil, err
	}

	updatedUser, err := GetUserByUsername(aUser.Username)

	if err != nil {
		panic(err.Error())
		return nil, err
	}

	return updatedUser, nil
}

func UsersGetHandler(w http.ResponseWriter, r *http.Request) {

	aCookie, err := r.Cookie("username")

	if err != nil {
		http.Error(w, http.StatusText(500), 500)
		return
	}

	aUser, err := GetUserByUsername(aCookie.Value)

	if err != nil {
		http.Error(w, http.StatusText(500), 500)
		return
	}

	data := &Page {
		PageTitle: "User profile",
		Data: aUser,
		LoggedOn: true,
	}

	t, _ := template.ParseFiles("templates/userProfileTplt.html")
	t.Execute(w, data)
}

func UsersLoginHandler(w http.ResponseWriter, r *http.Request) {

	r.ParseForm()

	username := r.FormValue("username")
	password := r.FormValue("password")

	authorized := Login(username, password)

	if authorized {

		expiration := time.Now().Add(24 * time.Hour)
		cookie := http.Cookie{
			Name: "username",
			Value: username,
			Expires: expiration,
			HttpOnly: true,
			Path: "/",
		}

		http.SetCookie(w, &cookie)
		http.Redirect(w, r, "/users/profile/", http.StatusSeeOther)
		return
	}
	http.Redirect(w, r, "/", http.StatusSeeOther)
}

func UsersLogoutHandler(w http.ResponseWriter, r *http.Request) {

	expiration := time.Now().Add(-96 * time.Hour)
	c := http.Cookie{
		Name:   "username",
		Value: "",
		MaxAge: -1,
		Expires: expiration,
		HttpOnly: true,
		Path: "/",
	}

	http.SetCookie(w, &c)
	http.Redirect(w, r, "/", http.StatusSeeOther)

}

func UsersUpdateHandler(w http.ResponseWriter, r *http.Request) {

	aUser := new(User)

	r.ParseForm()

	aUser.Id = r.FormValue("id")
	aUser.Username = r.FormValue("username")
	aUser.FullName = r.FormValue("fullname")
	aUser.Phone = r.FormValue("phone")
	aUser.Address = r.FormValue("google")
	aUser.Pass = r.FormValue("password")

	_, err := UpdateUser(aUser)

	if err != nil {
		fmt.Println("User cannot be updated. Please check.")
	}

	http.Redirect(w, r, "/users/profile/", http.StatusSeeOther)
}

func UsersInsertHandler(w http.ResponseWriter, r *http.Request) {

	aUser := new(User)

	r.ParseForm()

	aUser.Id = ""
	aUser.Username = r.FormValue("username")
	aUser.FullName = ""
	aUser.Phone = ""
	aUser.Address = ""
	aUser.Pass = r.FormValue("password")

	username, err := AddUser(aUser)

	if err != nil {
		fmt.Println("User cannot be inserted. Please check.")
	}

	expiration := time.Now().Add(24 * time.Hour)
	cookie := http.Cookie{
		Name: "username",
		Value: username,
		Expires: expiration,
		HttpOnly: true,
		Path: "/",
	}

	http.SetCookie(w, &cookie)
	http.Redirect(w, r, "/users/profile/", http.StatusSeeOther)
}