package main

import(
	"os"
	"fmt"
	"log"
	"io/ioutil"
	"net/http"
	"html/template"
	"login/models"
	"encoding/json"
	"encoding/base64"
	"crypto/rand"
	"golang.org/x/oauth2"
	"golang.org/x/oauth2/google"

/*    "golang.org/x/net/context"
    "golang.org/x/oauth2"
    "golang.org/x/oauth2/google"
    "google.golang.org/api/docs/v1"
*/
)

type Page struct{
	PageTitle string
	Data *models.User
}

type Credentials struct {
	Cid string `json:"cid"`
	Csecret string `json:"csecret"`
}

var cred Credentials
var conf *oauth2.Config
var state string


func homeHandler(w http.ResponseWriter, r *http.Request) {

	aUser := new(models.User)

	data := &Page {
		PageTitle: "Homepage!",
		Data: aUser,
	}

	switch r.Method {
		case "GET":
			state := randToken()
			w.Write([]byte("<html><title>Golang Google</title> <body> <a href='" + getLoginURL(state) + "'><button>Login with Google!</button> </a> </body></html>"))

			t, _ := template.ParseFiles("templates/index.html")
			t.Execute(w, data)
		case "POST":
			models.UsersLoginHandler(w, r)
		default:
			fmt.Println("Only allowed GET, PUT and POST methods.")
			http.Error(w, http.StatusText(405), 405)
	}
}

func usersRegisterHandler(w http.ResponseWriter, r *http.Request) {

	aUser := new(models.User)

	data := &Page {
		PageTitle: "Create new user!",
		Data: aUser,
	}

	switch r.Method {
		case "GET":
			t, _ := template.ParseFiles("templates/register.html")
			t.Execute(w, data)
		case "POST":
			models.UsersInsertHandler(w, r)
		default:
			fmt.Println("Only allowed GET method.")
			http.Error(w, http.StatusText(405), 405)
	}
}

func usersLogoutHandler(w http.ResponseWriter, r *http.Request) {

	switch r.Method {
		case "GET":
			models.UsersLogoutHandler(w, r)
		default:
			fmt.Println("Only allowed GET method.")
			http.Error(w, http.StatusText(405), 405)
	}
}

func usersProfileHandler(w http.ResponseWriter, r *http.Request) {

	switch r.Method {
		case "GET":
			models.UsersGetHandler(w, r)
		case "POST":
			models.UsersUpdateHandler(w, r)
		default:
			fmt.Println("Only allowed GET or POST methods.")
			http.Error(w, http.StatusText(405), 405)
	}
}

func init() {

	file, err := ioutil.ReadFile("oAuth/creds.json")

	if err != nil {
		log.Printf("File error: %v\n", err)
		os.Exit(1)
	}

	json.Unmarshal(file, &cred)

	conf = &oauth2.Config{
		ClientID:     cred.Cid,
		ClientSecret: cred.Csecret,
		RedirectURL:  "http://127.0.0.1:44300/",
		Scopes: []string{
			"https://www.googleapis.com/auth/userinfo.email",
		},
		Endpoint: google.Endpoint,
	}
}

func getLoginURL(state string) string {
	return conf.AuthCodeURL(state)
}

func randToken() string {

	b := make([]byte, 32)
	rand.Read(b)

	return base64.StdEncoding.EncodeToString(b)
}

func main() {

	models.InitDB("root:root@tcp(localhost:8889)/golang")

	http.HandleFunc("/", homeHandler)
	http.HandleFunc("/users/register/", usersRegisterHandler)
	http.HandleFunc("/users/profile/", usersProfileHandler)
	http.HandleFunc("/users/logout/", usersLogoutHandler)

	log.Fatal(http.ListenAndServe(":44300", nil))
}